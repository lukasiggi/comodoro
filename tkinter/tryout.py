""" Main script for the comodoro Pomodoro timer app.

    What we want:
    - a neat-looking background
    - a big tomato image
    - a timer with a corresponding progress bar
    - a proper way of configuration

    Useful resources:
    - event bindings and keystrokes:
      https://www.pythontutorial.net/tkinter/tkinter-event-binding/
"""
import tkinter as tk
import tkinter.messagebox
from tkinter import ttk
# import keyboard


# some settings, to be replaced by config.yaml
class Settings:
    width = 600
    height = 400
    alpha = 0.9
    icon = "../assets/comodoro.png"
    pomodoro_length_in_m = 25.0
    pause_length_in_m = 5.0
    bindings = {
        "ctrl+c+s": "start",
        "ctrl+c+t": "stop",
    }

cfg = Settings()


def main():
    # setup global keyboard bindings
    # setup_global_keyboard_bindings(cfg.bindings)

    # create main window
    root = tk.Tk()

    # set attributes of main window
    root.title("comodoro")

    # specify size of window
    root.geometry(get_center_coordinates(root, cfg.width, cfg.height))

    # make window partly transparent
    root.attributes('-alpha', cfg.alpha)

    # always place window on top of window stack
    root.attributes('-topmost', 1)

    # set icon of main window
    photo_image = tk.PhotoImage(file=cfg.icon)
    root.iconphoto(False, photo_image)

    # create all components of main window
    create_components(root)

    # run mainloop until GUI is closed
    root.mainloop()


def create_components(root):
    # way 1 of changing something: during creation
    # place a label on the root window
    message = tk.Label(root, text="Hi!")
    message.pack()

    # way 2: via dict syntax
    # place a label on the root window
    message["text"] = "Hi2"

    # way 3: via config()
    # place a label on the root window
    message.config(text="Hi3")

    # create start button. with a lambda expression we can pass arguments
    # there are 2 possible ways of binding:
    # 1. command binding --> only a few widgets support that, and it's limited
    # 2. event binding
    start_button = ttk.Button(root, text='Start', command=lambda: start_clicked({})).pack()
    start_button_2 = ttk.Button(root, text='Another_start')
    start_button_2.bind("<Control-s>", start_clicked)
    start_button_2.bind("<Control-s>", start_timer, add="+")  # add another listener
    start_button_2.focus()
    start_button_2.pack()

    # you can also bind events to classes
    # root.bind_class('Entry', '<Control-V>', paste)


def get_center_coordinates(root, width, height):
    # get the screen dimension
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # find the start point
    start_x = int(screen_width / 2 - width / 2)
    start_y = int(screen_height / 2 - height / 2)

    # return full geometry string
    return "%dx%d+%d+%d" % (width, height, start_x, start_y)


# def setup_global_keyboard_bindings(bindings):
#     keyboard.add_hotkey("ctrl+c+s", start_clicked, args=None)


def start_clicked(args):
    print('Button clicked')
    tkinter.messagebox.showinfo(
        title='Information',
        message='Start button clicked!'
    )


def start_timer(args):
    print("Timer started")


if __name__ == '__main__':
    main()