""" Main script for the comodoro Pomodoro timer app.

    What we want:
    - a neat-looking background
    - a big tomato image
    - a timer with a corresponding progress bar
    - a proper way of configuration

    Useful resources:
    - event bindings and keystrokes:
      https://www.pythontutorial.net/tkinter/tkinter-event-binding/
"""
import tkinter as tk
from tkinter import ttk
# import keyboard


# some settings, to be replaced by config.yaml
class Settings:
    width = 600
    height = 400
    alpha = 0.9
    icon = "../assets/comodoro.png"
    background_img = "../assets/pomodoro.png"
    pomodoro_length_in_m = 25.0
    pause_length_in_m = 5.0
    bindings = {
        "ctrl+c+s": "start",
        "ctrl+c+t": "stop",
    }

cfg = Settings()


def main():
    # setup global keyboard bindings
    # setup_global_keyboard_bindings(cfg.bindings)

    # create main window
    root = tk.Tk()

    # set attributes of main window
    root.title("comodoro")

    # use grid positioning
    root.grid()

    # specify size of window
    root.geometry(get_center_coordinates(root, cfg.width, cfg.height))

    # make window partly transparent
    root.attributes('-alpha', cfg.alpha)

    # always place window on top of window stack
    root.attributes('-topmost', 1)

    # set icon of main window
    photo_image = tk.PhotoImage(file=cfg.icon)
    root.iconphoto(False, photo_image)

    # create all components of main window
    create_components(root)

    # run mainloop until GUI is closed
    root.mainloop()


def create_components(root):
    # display background image
    photo = tk.PhotoImage(file=cfg.background_img)
    image_label = ttk.Label(
        root,
        image=photo,
        padding=5
    )
    image_label.grid(column=0, row=0)

    # progressbar
    pb = ttk.Progressbar(
        root,
        orient='horizontal',
        mode='determinate',
        length=280
    )
    # place the progressbar
    pb.grid(column=0, row=0, columnspan=2, padx=10, pady=20)

    # start button
    start_button = ttk.Button(
        root,
        text='Start',
        command=pb.start,
    )
    start_button.grid(column=0, row=1, padx=10, pady=10, sticky=tk.E)

    # stop button
    stop_button = ttk.Button(
        root,
        text='Stop',
        command=pb.stop
    )
    stop_button.grid(column=1, row=1, padx=10, pady=10, sticky=tk.W)



def get_center_coordinates(root, width, height):
    # get the screen dimension
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # find the start point
    start_x = int(screen_width / 2 - width / 2)
    start_y = int(screen_height / 2 - height / 2)

    # return full geometry string
    return "%dx%d+%d+%d" % (width, height, start_x, start_y)


# def setup_global_keyboard_bindings(bindings):
#     keyboard.add_hotkey("ctrl+c+s", start_clicked, args=None)


def start_clicked(args):
    print('Button clicked')


def start_timer(args):
    print("Timer started")




if __name__ == '__main__':
    main()